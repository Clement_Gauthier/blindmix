import spotipy
import random
import os
import tkinter as tk
import customtkinter
from spotipy.oauth2 import SpotifyClientCredentials
import pyautogui
import json

#############################################################VARIABLES
SPOTIFY_CLIENT_ID = "ba5be87420e84c6f81c2454064de4e85"
SPOTIFY_CLIENT_SECRET = "3dd7e0e4fce04a7d9719c9f326e890bf"

MAX_TRACKS_PER_PLAYLIST = 100

playlists = [
    
]

playlist_links = []

# Liste pour stocker les informations (titre, artiste, destinataire, URI Spotify/Deezer de la musique)
playlist_info = []
playlist_lengths = {}

current_track_title = ""
current_destinataire = ""

#############################################################CLASSES
class MyScrollableCheckboxFrame(customtkinter.CTkScrollableFrame):
    def __init__(self, master, title, values):
        super().__init__(master, label_text=title)
        self.grid_columnconfigure(0, weight=1)
        self.values = values
        self.checkboxes = []

        for i, value in enumerate(self.values):
            checkbox = customtkinter.CTkCheckBox(self, text=value)
            checkbox.grid(row=i, column=0, padx=10, pady=(10, 0), sticky="w")
            self.checkboxes.append(checkbox)

    def get(self):
        checked_checkboxes = []
        for checkbox in self.checkboxes:
            if checkbox.get() == 1:
                checked_checkboxes.append(checkbox.cget("text"))
        return checked_checkboxes

#############################################################FUNCTIONS
def save_playlists_to_json(playlists):
    with open('playlists.json', 'w', encoding='utf-8') as f:
        json.dump(playlists, f, ensure_ascii=False)

def load_playlists_from_json():
    try:
        with open('playlists.json', 'r', encoding='utf-8') as f:
            playlists = json.load(f)
            return playlists
    except FileNotFoundError:
        return []

def extract_track_info(track):
    track_name = track['track']['name']
    artist_name = track['track']['artists'][0]['name']
    track_uri = track['track']['uri']
    return track_name, artist_name, track_uri

def quit_program():
    window.quit()

def getPlayers():
    players = set()
    for link in playlists:
        destinataire, _ = link.split("_", 1)
        players.add(destinataire)
    return list(players)

def addPlaylist():
    windowAddPlaylist = customtkinter.CTk()
    windowAddPlaylist.geometry("800x200")
    windowAddPlaylist.grid_columnconfigure((0), weight=1)

    #Ajouter inputs
    frame = customtkinter.CTkFrame(windowAddPlaylist)
    frame.grid(row=0, column=0, pady=(0, 20), padx=0, sticky="nsew")
    inputPlaylist = customtkinter.CTkEntry(frame, placeholder_text="Playlist_URL", width=500)
    inputPlaylist.grid(row=0, column=0, pady=0, padx=(20, 20), sticky="ew")
    inputName = customtkinter.CTkEntry(frame, placeholder_text="Nom", width=200)
    inputName.grid(row=0, column=1, pady=20, padx=(20, 20), sticky="w")

    def newPlaylist():
        newlist = inputName.get()+"_"+inputPlaylist.get()
        playlists.append(newlist)
        windowAddPlaylist.destroy()
        create_menu()

    new_playlist = customtkinter.CTkButton(windowAddPlaylist, text="Ajouter Playlist", command=newPlaylist, font=("Verdana", 12))
    new_playlist.grid(row=1, column=0, padx=(10, 10), pady=0, sticky="nsew")

    windowAddPlaylist.mainloop()

def create_menu():
    app = customtkinter.CTk()
    app.geometry("600x400")
    app.grid_columnconfigure((0), weight=1)

    # En-tête avec le titre du jeu
    title_label = customtkinter.CTkLabel(app, text="BlindMix", font=("Verdana", 16))
    title_label.grid(row=0, column=0, columnspan=2, pady=20, padx=20, sticky="ew")

    app.playerFrame = MyScrollableCheckboxFrame(app, title="Joueurs", values=getPlayers())
    app.playerFrame.grid(row=0, column=0, padx=(10, 0), pady=(10, 10), sticky="nsew")

    def goAddPlaylist():
        app.destroy()
        addPlaylist()

    new_playlist = customtkinter.CTkButton(app, text="Ajouter Playlist", command=goAddPlaylist, font=("Verdana", 12))
    new_playlist.grid(row=0, column=1, padx=(10, 10), pady=(150, 150), sticky="nsew")

    def launchGame():
        #TODO Lire le fichier des playlists 
        for playlist in playlists:
            destinataire, _ = playlist.split("_", 1)
            if destinataire in app.playerFrame.get():
                playlist_links.append(playlist)
        app.destroy()
        mainGame()

    button_launch_game = customtkinter.CTkButton(app, text="Lancer le jeu", command=launchGame, font=("Verdana", 12))
    button_launch_game.grid(row=2, column=0, padx=10, pady=10, sticky="ew")

    app.mainloop()

def mainGame():
    auth_manager = SpotifyClientCredentials(client_id=SPOTIFY_CLIENT_ID, client_secret=SPOTIFY_CLIENT_SECRET)
    sp = spotipy.Spotify(auth_manager=auth_manager)
    all_track_info_list = []

    def get_all_playlist_tracks(playlist_link):
        tracks = []
        offset = 0
        limit = 100

        while True:
            results = sp.playlist_tracks(playlist_link, offset=offset, limit=limit)
            track_info_list = [extract_track_info(item) for item in results['items']]
            tracks.extend(track_info_list)

            if len(results['items']) < limit:
                break  # Il n'y a plus de pistes à récupérer, nous avons terminé.

            offset += limit

        return tracks

    # Récupérer toutes les pistes des playlists avec pagination
    for link in playlist_links:
        destinataire, playlist_link = link.split("_", 1)
        playlist_tracks = get_all_playlist_tracks(playlist_link)
        playlist_info.extend([(destinataire, *info) for info in playlist_tracks])

    # Interface Tkinter
    app = customtkinter.CTk()
    app.geometry("600x400")
    app.grid_columnconfigure((0), weight=1)

    title_label = customtkinter.CTkLabel(app, text="BlindMix", font=("Verdana", 16))
    title_label.grid(row=0, column=0, columnspan=2, pady=20, padx=20, sticky="ew")

    destinataire_label = customtkinter.CTkLabel(app, text=current_destinataire, font=("Verdana", 16))
    destinataire_label.grid(row=4, column=0, columnspan=2, pady=20, padx=20, sticky="ew")
    song_label = customtkinter.CTkLabel(app, text=current_track_title, font=("Verdana", 16))
    song_label.grid(row=5, column=0, columnspan=2, pady=20, padx=20, sticky="ew")

    def find_new_track():
        global current_track_title, current_destinataire  # Déclarer les variables comme globales
        random_track = random.choice(playlist_info)
        destinataire, title, artist, track_uri = random_track
        current_track_title = f"Titre : {title} - Artiste : {artist}"
        current_destinataire = f"Destinataire : {destinataire}"
        song_label.configure(text=current_track_title)  # Mettre à jour le texte du label
        destinataire_label.configure(text=current_destinataire)  # Mettre à jour le texte du label destinataire
        if destinataire_label.winfo_ismapped():
            toggle_destinataire()
        # Lancer l'application Spotify ou Deezer avec l'URI du morceau
        os.system(f"start {track_uri}")
        pyautogui.sleep(3)
        pyautogui.press("enter")

    
    # Fonction pour afficher ou cacher le nom du destinataire
    def toggle_destinataire():
        if destinataire_label.winfo_ismapped():  # Vérifier si le label est déjà visible
            destinataire_label.grid_forget()  # Cacher le label
            song_label.grid_forget()
        else:
            destinataire_label.grid(row=4, column=0, columnspan=2, pady=20, padx=20, sticky="ew")
            song_label.grid(row=5, column=0, columnspan=2, pady=20, padx=20, sticky="ew")

    button_launch_game = customtkinter.CTkButton(app, text="Nouveau titre", command=find_new_track, font=("Verdana", 12))
    button_launch_game.grid(row=2, column=0, padx=10, pady=10, sticky="ew")

    button_launch_game = customtkinter.CTkButton(app, text="Afficher joueur", command=toggle_destinataire, font=("Verdana", 12))
    button_launch_game.grid(row=3, column=0, padx=10, pady=10, sticky="ew")

    # Compter le nombre de titres par destinataire
    playlist_lengths = {}
    for destinataire, _, _, _ in playlist_info:
        if destinataire not in playlist_lengths:
            playlist_lengths[destinataire] = 0
        playlist_lengths[destinataire] += 1
    print(playlist_lengths)

    # Mettre la fenêtre toujours au premier plan
    app.wm_attributes("-topmost", 1)

    # Lancer la boucle Tkinter
    app.mainloop()

if __name__ == "__main__":
    playlists = load_playlists_from_json()

    create_menu()

    save_playlists_to_json(playlists)
